##@ [Docker]

.PHONY: docker-build
docker-build: ## Build the Docker image for production
	@docker build -t $(DOCKER_IMAGE_NAME):prod --target prod .

.PHONY: docker-run
docker-run: ## Run the latest Docker image
	@docker run \
		--detach \
		--restart always \
		--name boulzy-docker-hostmanager \
		-v /etc/hosts:/etc/hosts \
		-v /var/run/docker.sock:/var/run/docker.sock \
		$(DOCKER_IMAGE_NAME):0.1.2

.PHONY: docker-local
docker-local: ## Run the local production image
	@docker run \
		--rm \
		-v /etc/hosts:/etc/hosts \
		-v /var/run/docker.sock:/var/run/docker.sock \
		$(DOCKER_IMAGE_NAME):prod

##@ [Local Development]

.PHONY: build-dev
build-dev: ## Build the Docker image used for local development
	@echo "Building Docker image for local development..."
	@docker build -t $(DOCKER_IMAGE_NAME):dev --target dev .
	@echo "Image built: $(DOCKER_IMAGE_NAME):dev"

.PHONY: sh
sh: ## Run a Go container with the project mounted for local development
	@touch ./hosts
	@docker run \
		--rm \
		-it \
		-v ./hosts:/etc/hosts \
		-v /var/run/docker.sock:/var/run/docker.sock \
		-v $(PWD)/src:/usr/src/app \
		$(DOCKER_IMAGE_NAME):dev sh

.PHONY: run-dev
run-dev: ## Run the code
	@touch ./hosts
	@docker run \
		--rm \
		-it \
		-v ./hosts:/etc/hosts \
		-v /var/run/docker.sock:/var/run/docker.sock \
		-v $(PWD)/src:/usr/src/app \
		$(DOCKER_IMAGE_NAME):dev go run .

.PHONY: test
test: ## Run the tests
	@echo "Running tests suite..."
	@docker run \
		--rm \
		-it \
		$(DOCKER_IMAGE_NAME):dev go test

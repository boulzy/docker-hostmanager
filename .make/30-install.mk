##@ [Installation]

OS?=$(shell uname)
ifeq ($(OS),Darwin)
OS=darwin
endif
ifeq ($(OS),Linux)
OS=linux
endif
ifeq ($(OS),Windows_NT)
OS=windows
endif
OS_ARCH?=$(shell uname -m)
DOCKER_HOSTMANAGER?=$(shell which docker-hostmanager)

.PHONY: install
install: ## Install boulzy/docker-hostmanager on your system
ifeq ($(DOCKER_HOSTMANAGER),)
	@printf "Installing boulzy/docker-hostmanager...\n"
ifneq ($(OS),Windows)
	@printf "You may be asked for your password to install boulzy/docker-hostmanager\n"
	@sudo curl --location --output /usr/local/bin/docker-hostmanager "https://gitlab.com/boulzy/docker-hostmanager/-/releases/permalink/latest/downloads/binaries/docker-hostmanager-$(OS)-$(OS_ARCH)"
	@sudo chmod +x /usr/local/bin/docker-hostmanager
	@printf "boulzy/docker-hostmanager installed at /usr/local/bin/docker-hostmanager\n"
else
	@printf "Installation for Windows is not supported yet. Feel free to update this Makefile to provide installation for Windows.\n"
	exit 1
endif
else
	@printf "boulzy/docker-hostmanager already installed at $(DOCKER_HOSTMANAGER)\n"
endif

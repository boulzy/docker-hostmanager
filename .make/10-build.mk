##@ [Build]

.PHONY: build-bin
build-bin: ## Build the binaries
build-bin: prepare-build build-linux-arm64 build-linux-arm build-linux-amd64 build-darwin-arm64 build-darwin-amd64 build-windows-arm64 build-windows-amd64
	@echo "Binaries available in bin/"

.PHONY: prepare-build
prepare-build:
	@echo "Building binaries..."
	@docker build -q -t $(DOCKER_IMAGE_NAME):build --target base . &> /dev/null

.PHONY: build-linux-arm64
build-linux-arm64: ## Build binary for linux/arm64
	@docker run \
		--rm \
		-v ./bin/:/usr/src/app/bin \
		-e CGO_ENABLED=0 \
		-e GOOS=linux \
		-e GOARCH=arm64 \
		$(DOCKER_IMAGE_NAME):build \
		go build -o bin/docker-hostmanager-linux-arm64

.PHONY: build-linux-arm
build-linux-arm: ## Build binary for linux/arm
	@docker run \
		--rm \
		-v ./bin/:/usr/src/app/bin \
		-e CGO_ENABLED=0 \
		-e GOOS=linux \
		-e GOARCH=arm \
		$(DOCKER_IMAGE_NAME):build \
		go build -o bin/docker-hostmanager-linux-arm

.PHONY: build-linux-amd64
build-linux-amd64: ## Build binary for linux/amd64
	@docker run \
		--rm \
		-v ./bin/:/usr/src/app/bin \
		-e CGO_ENABLED=0 \
		-e GOOS=linux \
		-e GOARCH=amd64 \
		$(DOCKER_IMAGE_NAME):build \
		go build -o bin/docker-hostmanager-linux-amd64

.PHONY: build-darwin-arm64
build-darwin-arm64: ## Build binary for darwin/arm64
	@docker run \
		--rm \
		-v ./bin/:/usr/src/app/bin \
		-e CGO_ENABLED=0 \
		-e GOOS=darwin \
		-e GOARCH=arm64 \
		$(DOCKER_IMAGE_NAME):build \
		go build -o bin/docker-hostmanager-darwin-arm64

.PHONY: build-darwin-amd64
build-darwin-amd64: ## Build binary for darwin/amd64
	@docker run \
		--rm \
		-v ./bin/:/usr/src/app/bin \
		-e CGO_ENABLED=0 \
		-e GOOS=darwin \
		-e GOARCH=amd64 \
		$(DOCKER_IMAGE_NAME):build \
		go build -o bin/docker-hostmanager-darwin-amd64

.PHONY: build-windows-arm64
build-windows-arm64: ## Build binary for windows/arm64
	@docker run \
		--rm \
		-v ./bin/:/usr/src/app/bin \
		-e CGO_ENABLED=0 \
		-e GOOS=windows \
		-e GOARCH=arm64 \
		$(DOCKER_IMAGE_NAME):build \
		go build -o bin/docker-hostmanager-windows-arm64.exe

.PHONY: build-windows-amd64
build-windows-amd64: ## Build binary for windows/amd64
	@docker run \
		--rm \
		-v ./bin/:/usr/src/app/bin \
		-e CGO_ENABLED=0 \
		-e GOOS=windows \
		-e GOARCH=amd64 \
		$(DOCKER_IMAGE_NAME):build \
		go build -o bin/docker-hostmanager-windows-amd64.exe

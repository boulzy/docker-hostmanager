# Contributing

All contributions are welcome!


## Communication Channels

You can find help and discussion in the following places:

* GitLab Issues: <https://gitlab.com/boulzy/docker-hostmanager/-/issues>


## Reporting Bugs

Bugs are tracked in the project's [issue tracker].

When submitting a bug report, please include enough information to reproduce the
bug. A good bug report includes the following sections:

* Expected outcome
* Actual outcome
* Steps to reproduce, including sample code
* Any other information that will help debug and reproduce the issue, including
  stack traces, system/environment information, and screenshots

**Please do not include passwords or any personally identifiable information in
your bug report and sample code.**


## Merge Requests

Contributions to fix bugs or add new features are welcome!

Please check the issues to see if the feature/bug you want to work on is already
discussed or not. If no issue is open about your subject, it might be a good idea
to create an issue before involving yourself in some hard work, to make sure your
contribution fits in the context of this project.

When your feature/fix is ready, open a [merge request] and add a description of
the changes.

To be sure your merge request will be merged, follow these steps before opening
it:

* Respect the Git branch model (see below)
* Follow the project conventions (see below)
* Update the documentation if needed
* Add tests covering your feature/fix

The GitLab CI also needs to be OK for your merge request to be merged.


## Git conventions

This library follows the [Trunk Based Development] model.

### Adding a feature

All developments should be done on branches created from `master`, and be suject
to a merge-request in the `master` branch.

### Releasing a version

Tags are also directly created from the `master` branch, except for bug fixes on
previous releases. See section below for more details about the later.

### Fixing a bug

Bug fixes are done on the `master` branch.

If the fix must also be done on a previous version, follow this steps:

* Check if a release branch exist for the release (ex: `release/4.2`)
* If not, create the release branch from the tag (`git checkout -b release/4.2 4.2`)
* Fix the bug on `master`
* Cherry-pick the fix commit on the release branch
* Tag the release branch with the new version (`git tag 4.2.1`)


  [gitlab]: https://gitlab.com/boulzy/docker-hostmanager
  [Contributor Code of Conduct]: .CODE_OF_CONDUCT.md
  [issue tracker]: https://gitlab.com/Boulzy/docker-hostmanager/-/issues
  [merge request]: https://gitlab.com/Boulzy/docker-hostmanager/-/merge_requests
  [Trunk Based Development]: https://trunkbaseddevelopment.com/
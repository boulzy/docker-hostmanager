# boulzy/docker-hostmanager

**`boulzy/docker-hostmanager`** is a CLI tool to access your Docker containers
using domain names on a local environment.

🐳 To run as Docker container, please refer to the [Docker image](https://hub.docker.com/repository/docker/boulzy/docker-hostmanager)
documentation.

_Inspired by [iamluc/docker-hostmanager](https://hub.docker.com/r/iamluc/docker-hostmanager)_

-----

## ❔ What is **boulzy/docker-hostmanager**?

**boulzy/docker-hostmanager** is a binary written in Go that updates your /etc/hosts
to access your running Docker containers by domain names.

This is specifically useful for local development, to simulate domain names for
your web project, your database, or whatever container is running exposing ports.

## 💾 Installation

Installation instructions are located [here](./doc/install.md).

## ➡️ Usage

```shell
$ docker-hostmanager
```

The command will log in the standard output when the hosts file is updated,
and will log in the error output when an error occurs. Make sure that Docker
is running.

You can use the `--listen` option to make the command listen to Docker events
and keep the hosts file synchronized.

> To run once and forget about it, you can daemonize the command.
> * [Ubuntu](./doc/ubuntu-service.md)
> * [MacOS](./doc/mac-os-agent.md)
> * [Windows](./doc/windows-service.md)

### Start the command

```shell
$ docker-hostmanager --listen
```

The command will log when the host file is updated.

### Setup your Docker containers

A container is exposed if it is running and has ports binding.

For each exposed Docker container, the domain `$(CONTAINER_NAME).docker` will be
available by default. You can set additional domains using the following methods:

#### Using networks

For each network used by a container, a list of domain names corresponding to the
container name and the container aliases, suffixed by the network name.

```yaml
# docker-compose.yaml
services:
    nginx:
        container_name: 'nginx'
        networks:
            default:
            app:
                aliases:
                    - 'www'

networks:
    app:
        name: custom.local
```

The following domain names would be available (assuming the project name is my-app):

- nginx.custom.local
- nginx.my-app_default
- www.custom.local
- www.my-app_default
- `CONTAINER_ID[:10]`.custom.local
- `CONTAINER_ID[:10]`.my-app_default

#### Without network

If the container is not using any network, it is accessible by its container name
concatened with `.docker`.

You can set a `DOMAIN_NAME` environment variable in the running container to add one
one or more domains. To set multiple domains, separate their values by a comma:

```dotenv
# Domain app.example.local will be available
DOMAIN_NAME=app.example.local

# Domains www.example.local and app.example.local will be available for the container
DOMAIN_NAME=www.example.local,app.example.local
```

```shell
$ docker run --name my-app --rm -p '80:80' -e DOMAIN_NAME=www.example.local,app.example.local nginx
```

The following domain names would be available (assuming the project name is my-app):

- my-app.docker
- www.example.local 
- app.example.local

## ⚙️ Configuration

**boulzy/docker-hostmanager** requires a Docker API to talk to, such as a Docker
socket. It will use environment variables that should be define by Docker to
find the socket, so you shouldn't have to configure it. But if required, you
can set the following environment variables:

```dotenv
# Set the path to the Docker socket or the URL to the Docker server
DOCKER_HOST=/var/run/docker.sock
# Set the version of the API to use, leave empty for latest
DOCKER_API_VERSION=1.43
# Specify the directory from which to load the TLS certificates (ca.pem, cert.pem, key.pem)
DOCKER_CERT_PATH=
# Enable or disable TS verification (off by default)
DOCKER_TLS_VERIFY=on
```

By default, **boulzy/docker-hostmanager** will look for the hosts file on
`/etc/hosts` on Linux and MacOS, and `C:\windows\system32\drivers\etc\hosts` on
Windows.

You can specify a different hosts file using an environment variable:

```dotenv
DOCKER_HOSTMANAGER_HOSTS=/tmp/tests/hosts
```

## 🤔 Why not use iamluc/docker-hostmanager?

There are several reasons that pushed me to develop this project.

#### Usage on MacOS

On MacOS, Docker manages the `/etc/hosts` to handle networking for containers.
This means that mounting the `/etc/hosts` file will not work, the file on the
host will not be updated as expected.


While **iamluc/docker-hostmanager** provides a solution in the form of a PHAR
executable, I was curious about how to have a cross-platform solution that wouldn't
need any previous requirements, as I like to keep my host as clean as possible
and use Docker whenever I need to install an environment, such as Node.js or PHP.

#### Support for arm64 images

The **iamluc/docker-hostmanager** is currently not built to run on arm64
architecture. You can find more information about that [here](https://stackoverflow.com/a/71187360).

#### Having a go at Go

I was curious about Go for a while. I thought it would be a good use case for a
first project, and gave it a try.

## 🤝 Contributing

Contributions are welcome! Please read [CONTRIBUTING](CONTRIBUTING.md) for details.

## 📃 License

This project is licensed for use under the MIT License (MIT). Please see
[LICENSE](./LICENSE) for more information.

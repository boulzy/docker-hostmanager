# @see https://tech.davis-hansson.com/p/make/
# define the default shell
SHELL := bash

# OS is a defined variable for WIN systems, so "uname" will not be executed
# Values of OS:
#   Windows => Windows_NT
#   Mac 	=> Darwin
#   Linux 	=> Linux
OS?=$(shell uname -s)
ifeq ($(OS),Windows_NT)
# Windows requires the .exe extension, otherwise the entry is ignored
# @see https://stackoverflow.com/a/60318554/413531
SHELL := bash.exe
OS := windows
endif
ifeq ($(OS),Linux)
OS := linux
endif
ifeq ($(OS),Darwin)
OS := darwin
endif
OS_ARCH?=$(shell uname -m)

# @see https://tech.davis-hansson.com/p/make/ for some make best practices
# use bash strict mode @see http://redsymbol.net/articles/unofficial-bash-strict-mode/
# -e 			- instructs bash to immediately exit if any command has a non-zero exit status
# -u 			- a reference to any variable you haven't previously defined - with the exceptions of $* and $@ - is an error
# -o pipefail 	- if any command in a pipeline fails, that return code will be used as the return code
#				  of the whole pipeline. By default, the pipeline's return code is that of the last command - even if it succeeds.
# https://unix.stackexchange.com/a/179305
# -c            - Read and execute commands from string after processing the options. Otherwise, arguments are treated  as filed. Example:
#                 bash -c "echo foo" # will excecute "echo foo"
#                 bash "echo foo"    # will try to open the file named "echo foo" and execute it
.SHELLFLAGS := -euo pipefail -c
# display a warning if variables are used but not defined
MAKEFLAGS += --warn-undefined-variables
# remove some "magic make behavior"
MAKEFLAGS += --no-builtin-rules

# @see https://www.thapaliya.com/en/writings/well-documented-makefiles/
.DEFAULT_GOAL:=help

include .make/*.mk

# Variables
DOCKER_IMAGE_NAME := boulzy/docker-hostmanager

# Note:
# We are NOT using $(MAKEFILE_LIST) but defined the required make files manually via "Makefile .make/*.mk"
# because $(MAKEFILE_LIST) also contains the .env files AND we cannot force the order of the files
.PHONY: help
help:
	@awk 'BEGIN {FS = ":.*##"; printf "\n\033[1mUsage:\033[0m\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z0-9_-]+:.*?##/ { printf "  \033[36m%-40s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' Makefile .make/*.mk

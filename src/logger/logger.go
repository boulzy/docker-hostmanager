package logger

import (
	"os"
    "log"
)

var (
    infoLogger *log.Logger
    errorLogger *log.Logger
)

func init() {
	infoLogger = log.New(os.Stdout, "[INFO] ", log.Ldate|log.Ltime)
    errorLogger = log.New(os.Stderr, "[ERROR] ", log.Ldate|log.Ltime)
}

func Info(message string) {
	infoLogger.Println(message)
}

func Error(err error) {
	errorLogger.Println(err)
}

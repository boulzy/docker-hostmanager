package hostmanager

import (
	"os"
    "errors"
    "strings"
    "regexp"
    "golang.org/x/exp/slices"
    "gitlab.com/boulzy/dockerhostmanager/docker"
)

const StartTag = "###> boulzy/docker-hostmanager ###"
const EndTag = "###< boulzy/docker-hostmanager ###"

func UpdateHosts(hostsFile string) error {
	hostsContent, err := os.ReadFile(hostsFile)
    if err != nil {
        return err
    }

	hostsLines := strings.Split(string(hostsContent), "\n")
	start, err := findMatchingElement(hostsLines, StartTag)
	if err != nil {
		start = len(hostsLines) - 1
	}

	end, err := findMatchingElement(hostsLines, EndTag)
	if err != nil {
		end = len(hostsLines) - 1
	}

	dockerHosts, err := docker.GetDockerHosts()
	if (err != nil) {
		return err
	}

	dockerHosts = append([]string{StartTag}, dockerHosts...) // Prepend start tag
	dockerHosts = append(dockerHosts, EndTag)

	hostsLines = slices.Replace(hostsLines, start, end + 1, dockerHosts...)
	newContent := strings.Join(hostsLines, "\n")

	return os.WriteFile(hostsFile, []byte(newContent), 0644)
}

func findMatchingElement(lines []string, pattern string) (int, error) {
    regex, _ := regexp.Compile(pattern)

    for index, line := range lines {
        if regex.MatchString(line) {
            return index, nil
        }
    }

    return 0, errors.New("No matching line")
}

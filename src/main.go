package main

import (
	"os"
    "errors"
	"github.com/spf13/cobra"
    "gitlab.com/boulzy/dockerhostmanager/docker"
    "gitlab.com/boulzy/dockerhostmanager/hostmanager"
    "gitlab.com/boulzy/dockerhostmanager/logger"
)

var (
    listenEvents bool
    rootCmd = &cobra.Command{
        Use:  "docker-hostamanger",
        Short: "docker-hostmanager - a CLI command to synchronize your hosts file with running Docker containers",
        Long: `
    docker-hostmanager helps you access your Docker containers using domain names

    It edits the hosts file to keep it synchronized with running Docker containers.
    `,
        Args:  cobra.MaximumNArgs(1),
        Run: func(cmd *cobra.Command, args []string) {
            hostsFile, ok := os.LookupEnv("DOCKER_HOSTMANAGER_HOSTS")
            if !ok {
                hostsFile = defaultHostsFile
            }

            err := updateHostsFile(hostsFile)
            if nil != err {
                logger.Error(err)

                panic(err)
            }

            if false == listenEvents {
                return
            }

            err = docker.Listen(func() error {
                err := updateHostsFile(hostsFile)
                if nil != err {
                    return err
                }

                return nil
            })

            if nil != err {
                logger.Error(err)

                panic(err)
            }

        },
    }
)

func init() {
	rootCmd.PersistentFlags().BoolVarP(&listenEvents, "listen", "l", false, "Listen to Docker events to keep the hosts file synchronized")
}

func updateHostsFile(hostsFile string) error {
    err := hostmanager.UpdateHosts(hostsFile)
    if err != nil {
        return err
    }

    logger.Info(hostsFile + " updated")

    return nil
}

func main() {
    if err := rootCmd.Execute(); err != nil {
        logger.Error(errors.New("An unexpected error occured:"))
        logger.Error(err)
        os.Exit(1)
    }

    os.Exit(0)
}

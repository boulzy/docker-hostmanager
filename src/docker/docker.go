package docker

import (
	"context"
	"time"
	"strings"
	"io"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
    "gitlab.com/boulzy/dockerhostmanager/logger"
)

var dockerClient *client.Client

func init() {
	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if nil != err {
		panic(err)
	}

	dockerClient = cli
}

func GetDockerHosts() ([]string, error) {
	dockerHosts := make([]string, 0)

	for {
		containers, err := dockerClient.ContainerList(context.Background(), types.ContainerListOptions{All: false})
		if nil == err {
			for _, container := range containers {
				containerHosts, err := getContainerHosts(container.ID)
				if err != nil {
					return nil, err
				}
		
				for _, containerHost := range containerHosts {
					dockerHosts = append(dockerHosts, containerHost)
				}
			}
		
			return dockerHosts, nil
		}

		logger.Info("Couldn't connect to Docker. Retrying in 60s...")
		logger.Error(err)

		time.Sleep(60 * time.Second)
	}
}

func Listen(listener func() error) error {
	for {
		msgs, errs := dockerClient.Events(context.Background(), types.EventsOptions{})
		for {
			select {
				case err := <-errs:
					if nil != err && io.EOF == err {
						return nil
					}
	
					if nil != err {
						logger.Info("Couldn't connect to Docker. Retrying in 60s...")
						logger.Error(err)

						time.Sleep(10 * time.Second)
					}
				case msg := <-msgs:
					if "container" == msg.Type {
						err := listener()
						if nil != err {
							return err
						}
					}
			}
		}
	}
}

func getContainerHosts(id string) ([]string, error) {
	containerHosts := make([]string, 0)

	container, err := dockerClient.ContainerInspect(context.Background(), id)
	if nil != err {
		return nil, err
	}

	if !isExposed(container) {
		return nil, nil
	}

	containerDomains := make(map[string][]string)

	// Global
	if (0 < len(container.NetworkSettings.DefaultNetworkSettings.IPAddress)) {
		containerDomains[container.NetworkSettings.DefaultNetworkSettings.IPAddress] = getContainerDomains(container)
	}

	// Networks
	for networkName, network := range container.NetworkSettings.Networks {
		if 0 == len(network.Aliases) {
			continue
		}

		aliases := network.Aliases
		aliases = append(aliases, container.Name[1:])

		var domains []string
		for _, alias := range aliases {
			domains = append(domains, alias + "." + networkName)
		}
		containerDomains[network.IPAddress] = append(containerDomains[network.IPAddress], domains...)
	}

	for ip, domains := range containerDomains {
		containerHosts = append(containerHosts, ip + "\t" + strings.Join(removeDuplicateStr(domains), " "))
	}

	return containerHosts, nil
}

func getContainerDomains(container types.ContainerJSON) []string {
	hosts := make([]string, 0)

	hosts = append(hosts, container.Name[1:] + ".docker")
	for _, env := range container.Config.Env {
		if strings.HasPrefix(env, "DOMAIN_NAME=") {
			hosts = append(hosts, strings.Split(strings.ReplaceAll(env, "DOMAIN_NAME=", ""), ",")...)
		}
	}

	return hosts
}

func isExposed(container types.ContainerJSON) bool {
	return 0 < len(container.NetworkSettings.Ports)
}

func removeDuplicateStr(strSlice []string) []string {
    allKeys := make(map[string]bool)
    list := []string{}
    for _, item := range strSlice {
        if _, value := allKeys[item]; !value {
            allKeys[item] = true
            list = append(list, item)
        }
    }
    return list
}

FROM golang:1.21-alpine AS base

WORKDIR /usr/src/app

# pre-copy/cache go.mod for pre-downloading dependencies and only redownloading them in subsequent builds if they change
COPY src/go.mod src/go.sum ./
RUN go mod download
RUN go mod verify

COPY ./src .

FROM base AS dev

RUN apk update && apk add bash

FROM base AS ci

RUN apk update && apk add curl git

FROM base AS build

RUN GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -v -o /usr/local/bin/docker-hostmanager .


FROM scratch AS prod

COPY --from=build /usr/local/bin/docker-hostmanager /usr/local/bin/docker-hostmanager

ENTRYPOINT ["/usr/local/bin/docker-hostmanager", "--listen"]

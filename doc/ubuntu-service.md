# Run as a systemd service on Ubuntu

## Create a systemd User Service Unit File

Create a `~/.config/systemd/user/docker-hostmanager.service` file with the
following content:

```
[Unit]
Description=docker-hostmanager

[Service]
ExecStart=/usr/local/bin/docker-hostmanager
Restart=always
RestartSec=10

[Install]
WantedBy=default.target
```

## Enable and Start the systemd User Service

Reload the systemd user service manager to pick up the changes:

```bash
$ systemctl --user daemon-reload
```

Enable the user service to start at login:

```bash
$ systemctl --user enable docker-hostmanager.service
```

Replace myuseragent.service with the name you chose for your service.

Start the user service:

```bash
$ systemctl --user start docker-hostmanager.service
```

## Verify and Monitor the User Agent

To check the status of your user service:

```bash
$ systemctl --user status docker-hostmanager.service
```

If it's running, you'll see the status and other information.

To monitor your user agent's output and errors, you can use the journalctl command:

```bash
$ journalctl --user-unit docker-hostmanager.service
```

This will display logs related to your user service.


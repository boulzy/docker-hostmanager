# boulzy/docker-hostmanager

**`boulzy/docker-hostmanager`** allows you to access your Docker containers
using domain names on a local environment.

This is specifically useful for local development, to simulate domain names for
your web project, your database, or whatever container is running exposing ports.

> **On MacOS**, Docker manages the `/etc/hosts` to handle networking for containers.
> This means that mounting the `/etc/hosts` file will not work, the file on the
> host will not be updated as expected.
> See the [project](https://gitlab.com/boulzy/docker-hostmanager) documentation
> to see how to use **boulzy/docker-hostmanager** on MacOS.

> ⚠️ This project has not been tested on Windows.

_Inspired by [iamluc/docker-hostmanager](https://hub.docker.com/r/iamluc/docker-hostmanager)_

-----

## ➡️ Usage

**boulzy/docker-hostmanager** requires that you mount the Docker socket and your
hosts file. The Docker socket is usually located at `/var/run/docker.sock`.
The hosts file is located at `/etc/hosts` on Linux and MacOS, and on
`/c/Windows/System32/drivers/etc/hosts` on Windows.

```bash
$ docker run -d --rm --name docker-hostmanager --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v /etc/hosts:/hosts boulzy/docker-hostmanager
```

💻 To run as a command or a daemon, please refer to the [project](https://gitlab.com/boulzy/docker-hostmanager)
documentation.

## ⚙️ Configuration

A container is exposed if it is running and has ports binding.

For each exposed Docker container, the domain `$(CONTAINER_NAME).docker` will be
available by default. You can set additional domains using the following methods:

### Using networks

For each network used by a container, a list of domain names corresponding to the
container name and the container aliases, suffixed by the network name.

```yaml
# docker-compose.yaml
services:
    nginx:
        container_name: 'nginx'
        networks:
            default:
            app:
                aliases:
                    - 'www'

networks:
    app:
        name: custom.local
```

The nginx server would be available at the following URLs (assuming the project
name is my-app):

- http://nginx.custom.local
- http://nginx.my-app_default
- http://www.custom.local
- http://www.my-app_default
- http://`CONTAINER_ID[:10]`.custom.local
- http://`CONTAINER_ID[:10]`.my-app_default

### Without network

If the container is not using any network, it is accessible by its container name
concatened with `.docker`.

You can set a `DOMAIN_NAME` environment variable in the running container to add one
one or more domains. To set multiple domains, separate their values by a comma:

```dotenv
# Domain app.example.local will be available
DOMAIN_NAME=app.example.local

# Domains www.example.local and app.example.local will be available for the container
DOMAIN_NAME=www.example.local,app.example.local
```

```bash
$ docker run --name my-app --rm -p '80:80' -e DOMAIN_NAME=www.example.local,app.example.local nginx
```

The nginx server would be available at the following URLs:

- http://my-app.docker
- http://www.example.local 
- http://app.example.local

## 📃 License

This project is licensed for use under the MIT License (MIT). Please see
[LICENSE](https://gitlab.com/boulzy/docker-hostmanager/-/blob/master/LICENSE)
for more information.

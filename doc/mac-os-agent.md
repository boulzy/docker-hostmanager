# Run as an agent on MacOS

On MacOS, daemons and agents (per-user daemons) are managed by [`launchd`](https://support.apple.com/fr-fr/guide/terminal/apdc6c1077b-5d5d-4d35-9c19-60f2397b2369/mac).

## Create the `docker-hostmanager` job

```xml
<!-- ~/Library/LaunchAgents/local.docker-hostmanager.plist -->
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">

<plist version="1.0">
<dict>

    <key>Label</key>
    <string>local.docker-hostmanager</string>

    <key>DOCKER_HOSTMANAGER_HOSTS</key>
	<string>/private/etc/hosts</string>

    <key>Program</key>
    <string>/usr/local/bin/docker-hostmanager</string>

    <key>KeepAlive</key>
    <true/>

    <key>StandardOutPath</key>
    <string>/tmp/docker-hostmanager/docker-hostmanager.log</string>

    <key>StandardErrorPath</key>
    <string>/tmp/docker-hostmanager/docker-hostmanager.error.log</string>

</dict>
</plist>
```

## Load the Launch Agent

To load the launch agent, use the launchctl command:

 ```bash
$ launchctl load ~/Library/LaunchAgents/local.docker-hostmanager.plist
```

This command loads the plist file and starts your agent.

## Start, Stop, or Restart the Agent

To manually start the agent, use the launchctl start command:

```bash
$ launchctl start local.docker-hostmanager
```

To stop the agent, use the launchctl stop command:

```bash
$ launchctl stop local.docker-hostmanager
```

To restart the agent, stop it first and then start it again.

## Verify and Monitor the Agent

To verify that the agent is running, you can check its status with the launchctl
list command:

```bash
$ launchctl list | grep local.docker-hostmanager
```

If it's running, you'll see its PID (Process ID) listed.

To monitor your agent's output and errors, check the log files in
`/tmp/docker-hostmanager/`.

# 💾 Installation

## 📄 Summary

* [On MacOS](#mac-os)
* [On Linux](#linux)
* [On Windows](#windows)
* [Download from the GitLab releases page](#gitlab)

> ℹ️ You will need to know your host architecture to download the binary related
> to your platform. This can be done using the `uname` command, as seen below.
> 
> 

```bash
$ uname -s # Will display Darwin for MacOS, Linux for Linux and Windows_NT for Windows
$ uname -m # Will display arm64, arm or amd64
```

## <a name="mac-os"></a> 🍏 On MacOS

Download the binary in a directory available in the `$PATH` environment variable,
like the `/usr/local/bin` directory. You should have to run the command as `sudo``
to have the permission to write in this directory.

```bash
# Mac with Apple silicon
$ sudo curl --location --output /usr/local/bin/docker-hostmanager "https://gitlab.com/boulzy/docker-hostmanager/-/releases/permalink/latest/downloads/binaries/docker-hostmanager-darwin-arm64"

# Mac with Intel chip
$ sudo curl --location --output /usr/local/bin/docker-hostmanager "https://gitlab.com/boulzy/docker-hostmanager/-/releases/permalink/latest/downloads/binaries/docker-hostmanager-darwin-amd64"
```

If you downloaded the binary in a different directory, you should add the path of
this directory in the `$PATH` variable.

Run the chmod command to give the file the permission to be executed:

```bash
$ sudo chmod +x /usr/local/bin/docker-hostmanager
```

## <a name="linux"></a> 🐧 On Linux

Download the binary in a directory available in the `$PATH` environment variable,
like the `/usr/local/bin` directory. You should have to run the command as `sudo``
to have the permission to write in this directory.

```bash
# Linux with 64-bit ARM
$ sudo curl --location --output /usr/local/bin/docker-hostmanager "https://gitlab.com/boulzy/docker-hostmanager/-/releases/permalink/latest/downloads/binaries/docker-hostmanager-linux-arm64"

# Linux with 32-bit ARM
$ sudo curl --location --output /usr/local/bin/docker-hostmanager "https://gitlab.com/boulzy/docker-hostmanager/-/releases/permalink/latest/downloads/binaries/docker-hostmanager-linux-arm"

# Linux with 64-bit x86
$ sudo curl --location --output /usr/local/bin/docker-hostmanager "https://gitlab.com/boulzy/docker-hostmanager/-/releases/permalink/latest/downloads/binaries/docker-hostmanager-linux-amd64"
```

If you downloaded the binary in a different directory, you should add the path of
this directory in the `$PATH` variable.

Run the chmod command to give the file the permission to be executed:

```bash
$ sudo chmod +x /usr/local/bin/docker-hostmanager
```

## <a name="windows"></a> 🪟 On Windows

```bash
# Windows with 64-bit ARM
$ sudo curl --location --output /usr/local/bin/docker-hostmanager.exe "https://gitlab.com/boulzy/docker-hostmanager/-/releases/permalink/latest/downloads/binaries/docker-hostmanager-windows-arm64"

# Windows with 64-bit x86
$ sudo curl --location --output /usr/local/bin/docker-hostmanager.exe "https://gitlab.com/boulzy/docker-hostmanager/-/releases/permalink/latest/downloads/binaries/docker-hostmanager-windows-amd64"
```

## <a name="gitlab"></a> 🦊 Download from the GitLab releases page

You can find the latest release binaries for MacOS, Linux and Windows here:

https://gitlab.com/boulzy/docker-hostmanager/-/releases/

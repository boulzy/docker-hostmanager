# Run as a service on Windows

**You need to have the administrative access on your Windows machine.**

## Create the Windows Service Configuration File

Create a file `C:\ProgramData\docker-hostmanager\docker-hostmanager.xml` with administrative privileges.

```xml
<service>
    <id>docker-hostmanager</id>
    <name>docker-hostmanager</name>
    <description>Synchronize the hosts file with the running Docker containers</description>
    <executable>C:\usr\local\bin\docker-hostmanager\docker-hostmanager.exe</executable>
    <logpath>C:\usr\local\bin\docker-hostmanager\logs</logpath>
    <log mode="roll-by-size">
        <sizeThreshold>10240</sizeThreshold>
        <keepFiles>8</keepFiles>
    </log>
    <onfailure action="restart" delay="30 sec"/>
</service>
```

## Install the Service

Open a Command Prompt or PowerShell window with administrative privileges.

Use the sc (Service Control) command to create and start the service:

```
$ sc create docker-hostmanager binPath= "C:\usr\local\bin\docker-hostmanager.exe" type= own start= auto
```

Start the service:

```bash
$ sc start docker-hostmanager
```

## Configure Auto-Start on Login

Press Win + R, type shell:startup, and press Enter. This opens the Startup folder.

Create a shortcut to the sc start docker-hostmanager command in this folder.
